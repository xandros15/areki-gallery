<form enctype="multipart/form-data" method="post" role="form" action="/image/create">
    <input type="hidden" name="MAX_FILE_SIZE" value="20971520" />
    <div class="row">
        <h4 class="text-center"></h4>
        <div class="col-xs-12 form-group">
            <div class="col-xs-3">
                <label>Plik:</label>
            </div>
            <div class="col-xs-9">
                <input name="image" type="file">
            </div>
        </div>
        <div class="col-xs-12 form-group">
            <div class="col-xs-3">
                <label>Tytuł:</label>
            </div>
            <div class="col-xs-9">
                <input class="form-control" name="title" autocomplete="off">
            </div>
        </div>
        <div class="col-xs-12 form-group">
            <div class="col-xs-3">
                <label>Autor:</label>
            </div>
            <div class="col-xs-9">
                <input class="form-control" name="author" autocomplete="off">
            </div>
        </div>
        <div class="col-xs-12 form-group">
            <div class="col-xs-3">
                <label>Tekst do znaku wodnego:</label>
            </div>
            <div class="col-xs-9">
                <input class="form-control" name="watermark" autocomplete="off">
            </div>
        </div>
        <div class="col-xs-12 form-group">
            <input type="submit" class="btn btn-primary" role="button">
        </div>
    </div>
</form>