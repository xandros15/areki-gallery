<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title><?= $title ?></title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
        <style>
            .wrap{
                min-height: 100%;
                height: auto;
                margin: 0 auto -80px;
                padding: 0 0 80px;
            }
            html, body {
                height: 100%;
            }
            body > * {
                min-width: 480px;
            }
            .footer{
                height: 80px;
                background-color: #f5f5f5;
                border-top: 1px solid #ddd;
                padding: 20px;
            }
            .wrap > .container{
                padding: 70px 15px 20px;
                max-width: 960px;
            }
        </style>
    </head>
    <body>
        <div class="wrap">
            <?= render('nav') ?>
            <main role="main" class="container">
                 <?php alert()->display() ?>
                <?= $content ?>
            </main>
        </div>
        <footer class="footer"></footer>
        <!-- Latest compiled and minified JavaScript -->
        <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    </body>
</html>
