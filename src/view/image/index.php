<?php
/* @var $model MongoCollection */
?>
<div class="row">
    <?php foreach ($model as $image): ?>
        <div class="col-xs-6 col-md-3">
            <div class="thumbnail">
                <a href="/images/<?= $image['_id'] ?>/watermarked" class="thumbnail">
                    <img src="/images/<?= $image['_id'] ?>/tb" alt="...">
                </a>
                <div class="caption">
                    <h3><?= $image['title'] ?></h3>
                    <p><?= $image['author'] ?></p>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>