<?php

use Respect\Validation\Validator as v;
use Slim\Http\UploadedFile as File;
use Slim\Http\Request;

function showImageIndex() {
    $database = get_db();
    return $database->images->find();
}

function createImage(Request $request)
{
    $file          = $request->getUploadedFiles();
    $author        = $request->getParam('author');
    $title         = $request->getParam('title');
    $watermarkText = $request->getParam('watermark');

    validateImageAuthor($author);
    validateImageFile($file);
    validateImageTitle($title);
    validateImageWatermarkText($watermarkText);

    if (alert()->hasErrors()) {
        return false;
    }

    $database = get_db();
    /* @var $file File */

    /* @var $images MongoCollection */
    $images = $database->images;
    $image  = [
        'title' => $title,
        'author' => $author
    ];

    $images->insert($image);

    $foldername = createFolder(IMAGES_DIR . $image['_id']);

    $originalFilename = $foldername . 'original';

    $file['image']->moveTo($originalFilename);

    if (createImageThumbnail($originalFilename)) {
        alert()->success('Stworzono miniaturke');
    }

    if (createImageWatermark($originalFilename, $watermarkText)) {
        alert()->success('Stworzono obrazek ze znakiem wodnym');
    }

    alert()->success('Obrazek dodano poprawnie.');
    return true;
}

function createImageWatermark($originalFilename, $watermarkText)
{
    list($width, $height, $type) = getimagesize($originalFilename);

    switch ($type) {
        case IMAGETYPE_PNG:
            $image = imagecreatefrompng($originalFilename);
            break;
        case IMAGETYPE_JPEG:
            $image = imagecreatefromjpeg($originalFilename);
            break;
        default:
            return false;
    }

    $stamp = imagecreatetruecolor(strlen($watermarkText) * 10, 30);
    imagestring($stamp, 5, 5, 5, $watermarkText, 0x0000FF);

    imagecopymerge($image, $stamp, 10, $height - imagesy($stamp) - 10, 0, 0, imagesx($stamp), imagesy($stamp), 50);

    return imagejpeg($image, dirname($originalFilename) . DIRECTORY_SEPARATOR . 'watermarked');
}

function createImageThumbnail($originalFilename)
{
    list($width, $height, $type) = getimagesize($originalFilename);

    switch ($type) {
        case IMAGETYPE_PNG:
            $image = imagecreatefrompng($originalFilename);
            break;
        case IMAGETYPE_JPEG:
            $image = imagecreatefromjpeg($originalFilename);
            break;
        default:
            return false;
    }

    $tb = imagecreatetruecolor($width, $height);

    imagecopy($tb, $image, 0, 0, 0, 0, $width, $height);

    $tb = imagescale($tb, 200, 125);

    return imagejpeg($tb, dirname($originalFilename) . DIRECTORY_SEPARATOR . 'tb');
}

function validateImageFile($file)
{
    if (!isset($file['image']) || $file['image']->getError() === UPLOAD_ERR_NO_FILE) {
        return alert()->error('Musisz dodać plik.');
    }

    /* @var $file File */
    $file = $file['image'];

    if ($file->getError() === UPLOAD_ERR_INI_SIZE || $file->getError() === UPLOAD_ERR_FORM_SIZE) {
        alert()->error('Obrazek nie może być większy od 1MB, a mniejszy od 5KB.');
        return;
    }

    if (!v::size('5KB', '1MB')->validate($file->file)) {
        alert()->error('Obrazek nie może być większy od 1MB, a mniejszy od 5KB.');
    }

    if (!v::mimetype('image/png')->validate($file->file) && !($isJpg = v::mimetype('image/jpeg')->validate($file->file))) {
        alert()->error('Obrazek musi byc w formacie jpg lub png.');
    }
}

function validateImageAuthor($author)
{
    validateImageString('Autor', $author);
}

function validateImageTitle($title)
{
    validateImageString('Tytuł', $title);
}

function validateImageWatermarkText($watermarkText)
{
    validateImageString('Znak wodny', $watermarkText);
}

function validateImageString($argName, $string)
{
    if (!v::stringType()->notEmpty()->validate($string)) {
        alert()->error("{$argName} nie może być pusty.");
    }
    if (!v::stringType()->length(3, 20)->validate($string)) {
        alert()->error("{$argName} musi zawierać od 3 do 20 znaków.");
    }
}
