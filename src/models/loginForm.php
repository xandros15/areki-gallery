<?php

use Slim\Http\Request;
use Respect\Validation\Validator as v;

function siteRegisterNewUser(Request $request)
{
    $login          = $request->getParam('login');
    $email          = $request->getParam('email');
    $password       = $request->getParam('password');
    $passowrdRepeat = $request->getParam('passwordRepeat');

    validateSiteLogin($login);
    validateSiteEmail($email);
    validateSitePassword($password, $passowrdRepeat);

    if (alert()->hasErrors()) {
        return false;
    }

    return siteCreateUser($login, $email, $password);
}

function validateSiteLogin($login)
{
    $db = get_db();

    if (!v::alnum()->noWhiteSpace()->validate($login)) {
        alert()->error('Login może zawierać tylko znaki alfanumeryczne (bez spacji).');
    }

    if (!v::stringType()->length(5, 15)->validate($login)) {
        alert()->error("Login musi zawierać od 5 do 15 znaków.");
    }

    if ($db->users->findOne($login)) {
        alert()->error('Login zajęty.');
    }
}

function validateSiteEmail($email)
{
    if (!v::email()->validate($email)) {
        alert()->error('Niepoprawny adres email.');
    }
}

function validateSitePassword($password, $passwordRepeat)
{
    if (!v::stringType()->length(8, 20)->validate($password)) {
        alert()->error("Hasło musi zawierać od 8 do 20 znaków.");
    }

    if ($password != $passwordRepeat) {
        alert()->error('Hasło musi się zgadzać.');
    }
}

function siteCreateUser($login, $email, $password)
{
    $db = get_db();

    /* @var $users MongoCollection */
    $users = $db->users;
    $user  = [
        'login' => $login,
        'email' => $email,
        'password' => password_hash($password, PASSWORD_DEFAULT)
    ];

    if ($users->insert($user)) {
        alert()->success('Zostałeś zarejestrowany.');
        return true;
    }

    alert()->error('Błąd bazy danych.');
    return false;
}

function siteLoginUser(Request $request)
{
    $login    = $request->getParam('login');
    $password = $request->getParam('password');

    /* @var $users MongoCollection */
    $users = get_db()->users;

    $user = $users->findOne([
        'login' => (string) $login
    ]);

    if ($user && password_verify($password, $user['password'])) {
        login($user['email']);
        alert()->success('Jej, udało ci się zalogować.');
        return true;
    }

    alert()->error('Logowanie nie powiodło się.');
    return false;
}

function login($email)
{
    (session_id()) || @session_start();

    $_SESSION['user']['email']        = $email;
    $_SESSION['user']['login_status'] = 1;
}

function logout()
{
    unset($_SESSION['user']);
    alert()->success('Wylogowano poprawnie.');
}

function isLogged()
{
    return (isset($_SESSION['user']['login_status']) && $_SESSION['user']['login_status'] == 1);
}
