<?php

use Slim\Http\Request as Request;
use Slim\Http\Response as Response;
use Slim\App as Slim;
use Slim\Container;
use Plasticbrain\FlashMessages\FlashMessages;

/**
 * run program
 */
function run()
{
    $container = new Container(['settings' => [
            'displayErrorDetails' => true,
    ]]);

    $slim = new Slim($container);
    setRouting($slim);

    $slim->run();
}

function setRouting(Slim $slim)
{
    $slim->get('/', function ($request, $response) {
        return siteIndexAction($request, $response);
    });
    $slim->group('/image',
        function () {
        $this->get('[/]',
            function (Request $request, Response $response) {
            return imageIndexAction($request, $response);
        });
        $this->get('/form',
            function (Request $request, Response $response) {
            return imageFormAction($request, $response);
        });
        $this->post('/create',
            function (Request $request, Response $response) {
            return imageCreateAction($request, $response);
        });
    });
    $slim->map(['POST', 'GET'], '/login[/]',
        function ($request, $response) {
        return siteLoginAction($request, $response);
    });
    $slim->get('/logout[/]',
        function ($request, $response) {
        return siteLogoutAction($request, $response);
    });
    $slim->map(['POST', 'GET'], '/register[/]',
        function ($request, $response) {
        return siteRegisterAction($request, $response);
    });
}

function loadControllers()
{
    require_once CONTRL_DIR . 'siteController.php';
    require_once CONTRL_DIR . 'imageController.php';
}

function loadModels()
{
    require_once MODEL_DIR . 'image.php';
    require_once MODEL_DIR . 'loginForm.php';
}

/**
 * boot program
 */
function bootstrap()
{
    globalDefine();
    loadControllers();
    loadModels();
    setFlashMessages();
}

/**
 * define global const
 */
function globalDefine()
{
    defined('VIEW_DIR') || define('VIEW_DIR', __DIR__ . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR);
    defined('MODEL_DIR') || define('MODEL_DIR', __DIR__ . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR);
    defined('CONTRL_DIR') || define('CONTRL_DIR', __DIR__ . DIRECTORY_SEPARATOR . 'controllers' . DIRECTORY_SEPARATOR);
    defined('IMAGES_DIR') || define('IMAGES_DIR', ROOT_DIR . 'images' . DIRECTORY_SEPARATOR);
}

/**
 * render layout view
 *
 * @param string $layout
 * @param string $content
 * @param string $title
 * @return string
 */
function renderLayout($layout, $content, $title = '')
{
    return render($layout, ['title' => $title, 'content' => $content]);
}

/**
 * render view
 *
 * @param string $view
 * @param array $params
 * @return string
 */
function render($view, $params = [])
{
    $filename = str_replace('/', DIRECTORY_SEPARATOR, $view);
    ob_start();
    ob_implicit_flush(false);
    extract($params, EXTR_OVERWRITE);
    require (VIEW_DIR . $filename . '.php');
    return ob_get_clean();
}

/**
 * redirect to home
 *
 * @param Response $response
 * @return Request
 */
function goHome(Response $response)
{
    return $response->withRedirect('/', 301);
}

/**
 * Redirect to back
 *
 * @param Request $request
 * @param Response $response
 * @return Request
 */
function goBack(Request $request, Response $response)
{
    if ($request->hasHeader('HTTP_REFERER')) {
        $referer = $request->getHeader('HTTP_REFERER')[0];
        return $response->withRedirect($referer, 301);
    }
    return goHome($response);
}

/**
 * Create connection with db
 * 
 * @return MongoDB
 */
function get_db()
{
    $mongo = new Mongo("mongodb://localhost:27017/", [ 'db' => 'wai']);
    $db    = $mongo->wai;
    return $db;
}
$msg;

function setFlashMessages()
{
    global $msg;
    // Start a Session
    (session_id()) || @session_start();

    $msg = new FlashMessages();
}

/**
 * @global FlashMessages $msg
 * @return FlashMessages
 */
function alert()
{
    global $msg;
    return $msg;
}

function createFolder($folderName)
{
    if (!is_writable($folderName)) {
        mkdir($folderName, 755, true);
    }

    return $folderName . DIRECTORY_SEPARATOR;
}
