<?php

use Slim\Http\Request as Request;
use Slim\Http\Response as Response;

function imageCreateAction(Request $request, Response $response)
{
    if (!createImage($request)) {
        return goBack($request, $response);
    }
    return goHome($response);
}

function imageFormAction(Request $request, Response $response)
{
    $content = render('upload/_form');
    $response->getBody()->write(renderLayout('layout', $content, 'strona areki'));
    return $response;
}

function imageIndexAction(Request $request, Response $response)
{
    $content = render('image/index', ['model'=>  showImageIndex()]);
    $response->getBody()->write(renderLayout('layout', $content, 'indeks obrazków'));
    return $response;
}