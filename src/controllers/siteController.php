<?php

use Slim\Http\Request as Request;
use Slim\Http\Response as Response;

function siteIndexAction(Request $request, Response $response)
{
    $response->getBody()->write(renderLayout('layout', '', 'przykladowa strona areki'));
    return $response;
}

function siteLogoutAction(Request $request, Response $response)
{
    logout();
    return goHome($response);
}

function siteLoginAction(Request $request, Response $response)
{
    if (isLogged()) {
        alert()->warning('Jesteś już zalogowany.');
        return goHome($response);
    }
    if ($request->isGet()) {
        $content = render('login');
        return $response->getBody()->write(renderLayout('layout', $content, 'Logowanie'));
    }
    if ($request->isPost()) {
        if (!siteLoginUser($request)) {
            return goBack($request, $response);
        }
        return goHome($response);
    }
}

function siteRegisterAction(Request $request, Response $response)
{
    if (isLogged()) {
        alert()->warning('Osoba zalogowana nie może się zarejestrować.');
        return goHome($response);
    }
    if ($request->isGet()) {
        $content = render('register');
        return $response->getBody()->write(renderLayout('layout', $content, 'Rejestracja'));
    }
    if ($request->isPost()) {
        if (!siteRegisterNewUser($request)) {
            return goBack($request, $response);
        }
        return goHome($response);
    }
}
